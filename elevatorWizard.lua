-- wget https://gitlab.com/ccprojects1/create-elevator/-/raw/main/elevatorWizard.lua elevatorWizard

local args =  {...}

local download = true
local update = false

if args then
    local argCounter = 1
    repeat
        if args[argCounter] then
            if args[argCounter] == "nodownload" or args[argCounter] == "nd" then
                download = false
            elseif args[argCounter] == "update" or args[argCounter] == "up" then
                update = true
            end
        end
        argCounter = argCounter + 1
    until argCounter > 2
end

local baseURL = "https://gitlab.com/ccprojects1/create-elevator/-/raw/main/"

function getScript(url, fileName)
    local cacheBreak = tostring(math.random(0, 99999))
    
    url = url.."?breaker="..cacheBreak
    
    local res, err = http.get(url)
    if not res then error(err..'\n'..fileName..'\n'..url) end
    
    if fs.exists(fileName) then fs.delete(fileName) end
    if fs.exists(fileName..".lua") then fs.delete(fileName..".lua") end
    
    shell.run("wget", url, fileName)
end

local function clearScrn()
    term.clear()
    term.setCursorPos(1, 1)
end

if download then
    getScript(baseURL.."wizardData.lua", "wizardData")
end

--local scripts, questions, options = require("wizardData") or error("Error: Failed to download 'elevatorScriptLocations.lua'")
local questions = require("wizardData") or error("Error: Failed to get 'wizardData'")

clearScrn()
print("Welcome to the Elevator Wizard!\nPlease follow the instructions, or hold control + T for a few seconds to cancel and exit.\n")

local startupLine = "shell.run('"
local numQuestions = 1
local curQuest = 1

repeat
    if curQuest == 1 then
        local data = {questions[curQuest]()}
        local dir
        
        for counter, dat in ipairs(data) do
            if counter == 1 then
                if not update then
                    numQuestions = dat
                end
            elseif counter == 2 then
                dir = dat
            elseif counter == 3 then
                startupLine = startupLine..dat.."'"
                getScript(baseURL..dir..dat..".lua", dat)
            else
                getScript(baseURL..dir..dat..".lua", dat)
            end
        end
    else
        local param = questions[curQuest]()
        startupLine = startupLine..", '"..param.."'"
    end
    curQuest = curQuest + 1
until curQuest > numQuestions

if not update then
    startupLine = startupLine..")"
    
    if fs.exists("startup") then fs.delete("startup") end
    if fs.exists("startup.lua") then fs.delete("startup.lua") end

    startupFile = fs.open("startup", "w")
    startupFile.write(startupLine)
    startupFile.close()
end