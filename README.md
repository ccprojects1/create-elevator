## Elevator

This is an elevator which uses the CC:Tweaked and Create mods. I made it for Fun™️.

# Setup Guide

### You will need:
 - __2 computers__, plus __1 additional computer__ for **every floor**
 - **Modems connecting all computers** in the elevator together (wired or wireless)
 - **2 pistons for each floor** except the very top and very bottom floors
 - Some **redstone**, **redstone torches**, redstone **repeaters**
 - **1 Rope pulley** to move the elevator up/down
 - **1 Gearshift** to change direction of rope pulley
 - Adequate **generator(s)** and **cogs**  to operate the pulley at desired speed
 - Some **super glue** to keep the elevator in one piece

### Step one: The pulley
Place rope pulley in the desired elevator location. It will move the block underneath it. You will need at a minimum two other blocks connected to this main block. One to stand on, and one to interface with the pistons in order to stop at the desired floor.
I recommend building a more substantial platform and fencing off the block attached to the rope pulley.
I also recommend building a ceiling for your elevator if you're operating near or at maximum speed. Falling damage is possible at high speeds when the elevator moves downwards.

### Step two: The generators
Place your desired generator(s) and cog(s) in order to spin the pulley at your desired speed.
Maximum speed can be achieved with 4 water wheels, 4 large cogs, and 4 small cogs.

### Step three: The shaft
Once your elevator is reasonably planned out and looks good enough, it's time to make the elevator shaft. Dig a **hole in the shape of your elevator** to the desired depth. The shaft must be **free of any obstructions to the elevator**, otherwise it will not be able to freely travel through the entire shaft. It's **okay to place torches** on the shaft walls. The elevator will travel through those. If your elevator shaft is long enough to die if you fall, I **recommend placing water at the bottom.**
NOTE: If your elevator travels all the way to the bottom-most point of the shaft, either intentionally or by accident, be prepared for the elevator to destroy the water source blocks it lands inside. Or, be prepared for waterloggable blocks on your elevator floor to be waterlogged, and for the elevator to flood. One simple solution is to place a permanent stopper block just high enough so that the elevator would stop before hitting the water.

### Step four: Computers OMGOSH
At this point, it wouldn't hurt to test the motion of the elevator by simply placing a lever on the gearshift.

#### Okay, time for the computer stuff
To download the necessary software to the computers, paste the this command into them:
```txt
wget https://gitlab.com/ccprojects1/create-elevator/-/raw/main/elevatorWizard.lua elevatorWizard
```
This will download the setup wizard. Please follow the directions on this page carefully.

**All the computers need a protocol** to communicate. **The wizard will ask you** what you want it to be for each computer. It can be text or numbers or whatever you want (mostly), as long as **all computers within this elevator use the same protocol.**
Please ensure that it is **totally unique** to all other rednet protocols being used in the world/server. If you are on multiplayer, you could **begin your protocols with your username** to ensure uniqueness if you desire.

**Some computers require further information** from you, but I will let you know when that is the case.

To **start the wizard, type** `elevatorWizard` into the computer and press enter.

After completing the wizard, it should download the necessary scripts, and **generate a file named** `startup`. The startup file will contain the necessary configuration for the computer. This script will automatically run whenever the computer turns on. You can **run it manually by typing 'startup'** into the computer, **or just reboot** the computer by holding `ctrl + T` for a few moments, then opening the computer again.

Tip: In the future, if you want to update to the latest version of the scripts without going through the entire wizard again, you can run `elevatorWizard update` or `elevatorWizard up`. It will only ask you which type of computer you're on, and it will keep your current configuration.

1. The first computer will be the **controller computer**, aka **option 1** in the wizard.
	- Will be placed **on or near the gearshift.**
	 - The computer will **control the gearshift via redstone output from the bottom**
	 - It **probably won't work with repeaters or redstone torches** extending or inverting the signal. It uses quite fast redstone pulses to ensure reliability.

2. The second type of computer will be the **floor computers**, aka **option 2**.
	- You will need **one of these for every floor.**
	- The wizard will ask you for a **floor name**, which can be whatever you want, though this elevator **must not have 2 floors with the same name**.
	- Next, you will be asked for a **floor level**. This will be the **Y-coordinate** that your floor is on. It's okay if it's a block or two off, it's just used by the controller to know which direction to move you to get to the desired floor.
	- They will **control 2 pistons** by using **redstone output on the top and bottom**
	- The pistons **must be placed along the wall of the shaft just one block above, and one block below, the level at which you want your elevator to stop.**
	- The pistons must be **flush with the wall when un-powered**, and stick out to **block the elevator's movement when powered**. This is what I meant earlier about a block on the elevator interfacing with the pistons.
	- Wire the computer to the pistons, and **ensure it has a modem**.
	- NOTE: The floor computers on the **top floor and bottom floor do not need pistons**. A permanent block can be used to stop the elevator instead.
	- This computer **REQUIRES** there to be a **redstone signal going into the front** of the computer **when the elevator is on the same floor**. I recommend placing a redstone torch on the side of the block which the rope pulley is attached to, and creating wires on each floor going into the computer such that, if the elevator is present, its redstone torch will send a signal to the computer.
	- You're going to want to **reboot the controller computer** after ALL of these are set up.

3. The third type of computer will be the **elevator computer**, aka **option 3**
	- This computer will be **glued to the elevator**.
	- It will need to be **accessible** by the people **inside the elevator**.
	- It, of course, needs a **modem**. But, most modems have a habit of **breaking off when the elevator moves**. Sometimes they stay, but you'll need to experiment. My experience is that they **stay on if they're placed on top** of the computer. The **full-block wired modem** seems to **work with glue just fine**. A fully wired network will work fine, but if you prefer wireless, then **wired modems with wireless modems attached** can be placed at every floor, such that a cable coming out of the elevator will connect the elevator computer to a different wireless modem at every floor.
	- This script could be run on a **wireless pocket computer** if desired.

4. The fourth type of computer is the **caller computer**. it is **completely optional**.
	- This will be **placed on each floor** or as desired, and simply calls the elevator to one location.
	- The wizard will ask you for a **floor name**, like with the floor computer. The name **must be EXACTLY THE SAME as the one you entered into the floor computer**.

Whew. After you've done all of this, your elevator ***SHOULD*** be ready to use. Cross your fingers and pray you didn't accidentally miss a step.

### Step five: how to use
The only computers you need to interface with should be elevator computer and the caller computers. 
-	In the **elevator computer**, just **type the name of the floor** you want to go to, and press enter.
-	For the **caller computers**, just **press** `enter` to call the elevator to its configured floor.
-	If you ever **add a floor** to your existing elevator, just remember to **reboot the controller computer**.

### Step six: **B O N U S** (doors)
The elevator computer also has a redstone interface. It outputs at the bottom, and it can be used to control **elevator doors**. **Iron doors** work quite well for this.

The elevator computer can be used to control **doors glued to the elevator itself, and/or doors on the floor**, so that people don't accidentally walk into an elevator shaft with no elevator present.

Since the **redstone** signal comes from the **bottom of the computer**, I've found the easiest way is to place it on the floor of the elevator, and **underneath the elevator floor,** build/**glue a small structure with the desired redstone** circuitry to carry the signal to your doors.