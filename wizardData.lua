local function clearScrn()
    term.clear()
    term.setCursorPos(1, 1)
end

local function confirmInput(input)
    local confirm
    print("Just to confirm:")
    print(input)
    print("Does this look right?")
    print("[1] - That is correct")
    print("[2] - That is NOT correct")
    repeat
        confirm = tonumber(read())
    until confirm and (confirm == 1 or confirm == 2)
    
    if confirm == 2 then
        return false
    elseif confirm == 1 then
        return true
    end
end

local scriptDirectories = {
    "code/"
}

local scriptLocations = {
    "controllerv4",
    "floorComputerv2",
    "elevatorComputerv2",
    "elevatorCaller",
}

local questions = {}

questions[1] = function()
    print("What will this computer be used for?")
    print("Make a selection by typing the desired number and pressing 'enter'.")
    print("[1] - Main controller computer")
    print("[2] - Floor piston control computer")
    print("[3] - Onboard elevator terminal computer")
    print("[4] - Computer that calls the elevator to current floor")
    
    local input
    repeat
        input = tonumber(read())
    until input and input >= 1 and input <= 4
    
    if input == 1 then return 2, scriptDirectories[1], scriptLocations[1] end
    if input == 2 then return 4, scriptDirectories[1], scriptLocations[2] end
    if input == 3 then return 2, scriptDirectories[1], scriptLocations[3] end
    if input == 4 then return 3, scriptDirectories[1], scriptLocations[4] end
end

questions[2] = function()
    clearScrn()
    print("Your elevator needs a unique protocol to communicate over. An example would be 'myhouse' or 'mineshaft1' or something.")
    print("Every computer you set up for THIS elevator must use the same protocol.")
    print("Every OTHER ELEVATOR in the entire world must NOT use the same protocol.")
    print("NOTE: If you're on a multiplayer server, it might be a good idea to put your username at the start of all of your elevator protocols. For example, 'bob123-shaft1'. This would make sure you and all other users have unique protocols.")
    print("Please type what you would like to name your protocol and hit enter:")
    
    local protocol
    local confirm = false
    repeat
        repeat
            protocol = tostring(read())
        until protocol
        confirm = confirmInput(protocol)
        clearScrn()
        if not confirm then
            print("Let's try again. Enter your elevator's unique protocol now.")
        end
    until confirm == true
    
    return tostring(protocol)
end

questions[3] = function()
    clearScrn()
    print("The floor you're setting up needs a name. Some examples include 'basement', 'iron', 'subbasement', 'ground', '16', 'bedrock'")
    print("There may be other computers on THIS floor which ask your for a floor name. If they ask, the MUST be given EXACTLY the same floor name.")
    print("ALL floors in this elevator must have unique names. But two separate elevators could both have a floor that shares a name.")
    print("For example, each elevator can only have one floor named 'basement', but it's okay if multiple elevators have a floor named 'basement'.")
    print("Please input the floor name and press 'enter':")
    
    local name
    local confirm = false
    repeat
        repeat
            name = tostring(read())
        until name
        confirm = confirmInput(name)
        clearScrn()
        if not confirm then
            print("Let's try again. Enter this floor's name now:")
        end
    until confirm == true
    
    return tostring(name)
end

questions[4] = function()
    print("Please enter the Y-coordinate that this floor is on:")
    
    local level
    local confirm = false
    repeat
        repeat
            level = tonumber(read())
        until level
        confirm = confirmInput(level)
        clearScrn()
        if not confirm then
            print("Okay, please input the Y-coordinate that this floor is on:")
        end
    until confirm == true
    
    return tostring(level)
end

return questions