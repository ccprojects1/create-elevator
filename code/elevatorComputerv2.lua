-- shell.run("elevatorComputerv2", "protocol")

local args = {...}

local protocol = args[1] or error("Error: No protocol provided.")
local floorProtocol = protocol.."-floors"

peripheral.find("modem", rednet.open)

local elevatorCodes = {
    [30] = function(id) end, -- Who is everyone?
    [31] = function(id) end, -- Where is the elevator?
    [32] = function(id) end, -- Affirmative / I'm listening
    [33] = function(id) end, -- Send me the floor string
    [34] = function(id) end, -- Who's the controller?
    [35] = function(id) end, -- The elevator is at my location
    [36] = function(id) end, -- Hey, you should reboot
    [37] = function(id) end, -- Elevator is already at requested floor
    [38] = function(id) end, -- Elevator is busy
    [39] = function(id) end, -- Floor disabled or doesn't exist
}

local function clearScrn()
    term.clear()
    term.setCursorPos(1, 1)
end

local function getFloors()
    rednet.broadcast(33, protocol)
    local id, msg
    local counter = 1
    repeat
        id, msg = rednet.receive(protocol, 1)
        counter = counter + 1
    until msg or counter > 3
    return msg
end

local floorString

clearScrn()
print("Gathering floor data...")
floorString = getFloors()

clearScrn()
sleep(0.2)

while true do
    if floorString then print(floorString) end
    print("Which floor would you like to go to?")
    
    local input
    repeat
        input = tostring(read())
    until input
    
    if input == "/resetfloors" or input == "/resetFloors" then
        clearScrn()
        print("Telling all floors to reset...")
        rednet.broadcast("reset", floorProtocol)
        sleep(0.5)
    end
    
    if input == "/reboot" then
        clearScrn()
        print("Rebooting controller...")
        
        peripheral.find("modem", rednet.open)
        
        rednet.broadcast(36, protocol)
        sleep(3.5)
        clearScrn()
    end
    
    if input == "/reboot" or input == "/refresh" then
        clearScrn()
        print("Refreshing floor data...")
        
        peripheral.find("modem", rednet.open)
        
        local floors = getFloors()
        if floors then
            print("Done!")
            sleep(0.2)
            floorString = floors
        else
            floorString = "Timed out trying to get data. Is the controller running?"
        end
        clearScrn()
    else
        clearScrn()
        rednet.broadcast(input,  protocol)
        local id, msg = rednet.receive(protocol, 4)
        print(id, msg)
    end
end